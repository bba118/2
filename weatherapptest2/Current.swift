//
//  Current.swift
//  weatherapptest2
//
//  Created by ZSC on 12/11/2014.
//  Copyright (c) 2014 ZSC. All rights reserved.
//

import Foundation

struct Current {
    
    var currentTime: String?
    var temperature: Int
    var humidity: Double
    var precipProbability: Double
    var summary: String
    var icon: String
    
    init(weatherDictionary: NSDictionary) {
        let currentWeather = weatherDictionary["currently"] as NSDictionary
        
        temperature = currentWeather["temperature"] as Int
        humidity = currentWeather["humidity"] as Double
        precipProbability = currentWeather["precipProbability"] as Double
        summary = currentWeather["summary"] as String
        icon = currentWeather["icon"] as String
        
        let CurrenttimeIntValue = currentWeather["time"] as Int
        currentTime = dateStringFromUnixtime(CurrenttimeIntValue)
    }
    
    func dateStringFromUnixtime(unixTime: Int) -> String {
        let timeInSeconds = NSTimeInterval(unixTime)
        let weatherDate = NSDate(timeIntervalSince1970: timeInSeconds)
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .MediumStyle
        dateFormatter.dateStyle = .ShortStyle
        
        return dateFormatter.stringFromDate(weatherDate)
    }
    
    
    
    
    
    
}
